//Функции нужны чтобы не прописовать один и тот же кусок кода,а записать его всего лищь один раз и при надобности вызывать его.Функции это "строительный блок" программы.
//С помощью аргумента мы можем передавать функции информацию. Изменения видны только внутри функции.
// когда вызываем return, мы останавливаем выполнение функции и оно возращает её значение.

let numberFirst = +prompt("Введите первое число");
let numberSecond = +prompt("Введите второе число");
let operator = prompt("Введите знак операции");

// function calcResult(numberFirst, numberSecond, operator) {
//   return eval(`${numberFirst}` + operator + `${numberSecond}`);
// }

// console.log(calcResult(numberFirst, numberSecond, operator));
function calcResult(numberFirst, numberSecond, operator) {
  switch (operator) {
    case "+":
      return numberFirst + numberSecond;

    case "-":
      return numberFirst - numberSecond;

    case "*":
      return numberFirst * numberSecond;

    case "/":
      return numberFirst / numberSecond;
  }
}

console.log(calcResult(numberFirst, numberSecond, operator));
